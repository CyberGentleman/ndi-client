// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebase: {
        apiKey: 'AIzaSyDwGj0lpqaiiYo9624lsrDnbajfv0wPMb4',

        authDomain: 'snsm-archives.firebaseapp.com',

        projectId: 'snsm-archives',

        storageBucket: 'snsm-archives.appspot.com',

        messagingSenderId: '667869751558',

        appId: '1:667869751558:web:1e8e4d399c33a271d11687',

        measurementId: 'G-R4X576W84V'


    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
