
import {NgModule} from '@angular/core';
import {NgxScrollTopModule} from 'ngx-scrolltop';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from '../pages/not-found/not-found.component';
import { SauveteursListComponent} from './sauveteurs-list/sauveteurs-list.component';
import {CommonModule} from '@angular/common';
import {SauveuteursListService} from './sauveteurs-list/sauveuteurs-list.service';
import { HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
    {path: '', component: SauveteursListComponent},
    {path: '**', component: NotFoundComponent} // This line will remain down from the whole pages component list
];

@NgModule({
    declarations: [SauveteursListComponent],
    imports: [
        CommonModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        NgxScrollTopModule, HttpClientModule
    ],
    providers: [SauveuteursListService]
})
export class SauveteursModule {
}
