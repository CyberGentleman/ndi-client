import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class SauvetagesListService {

    constructor(private httpClient: HttpClient) {

    }

    getAllSauvetages() {
        return this.httpClient.get<any>('/api/sauvetage.php');
    }

    add(_body) {
        const body = new URLSearchParams();
        body.set('patronyme', _body.patronyme);
        body.set('nom', _body.nom);
        body.set('prenom', _body.prenom);
        body.set('date_de_naissance', _body.date_de_naissance);
        body.set('date_de_mort', _body.date_de_mort);
        body.set('fonction', _body.fonction);
        body.set('divers', _body.divers);
        const headers = { 'Content-Type': 'application/x-www-form-urlencoded'};
        return this.httpClient.post<any>('/api/sauveteur.php', body.toString(), {headers});
    }

    edit(_body) {
        console.log(_body.id)
        const body = new URLSearchParams();
        body.set('id', _body.id);
        body.set('patronyme', _body.patronyme);
        body.set('nom', _body.nom);
        body.set('prenom', _body.prenom);
        body.set('date_de_naissance', _body.date_de_naissance);
        body.set('date_de_mort', _body.date_de_mort);
        body.set('fonction', _body.fonction);
        body.set('divers', _body.divers);
        const headers = { 'Content-Type': 'application/x-www-form-urlencoded'};
        return this.httpClient.put<any>('/api/sauveteur.php', body.toString(), {headers});
    }

    delete(_body) {
        console.log(_body.ID)
        const body = new URLSearchParams();
        body.set('id', _body.ID);
        const headers = { 'Content-Type': 'application/x-www-form-urlencoded'};
        return this.httpClient.delete<any>('/api/sauveteur.php?id=' + _body.ID);
    }

}
