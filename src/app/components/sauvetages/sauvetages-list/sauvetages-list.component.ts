import {Component, OnInit} from '@angular/core';
import {SauvetagesListService} from './sauvetages-list.service';
import {AuthService} from '../../common/users/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-blog2-grid-page',
    templateUrl: './sauvetages-list.component.html',
    styleUrls: ['./sauvetages-list.component.scss']
})
export class SauvetagesListComponent implements OnInit {

    sauveteurList;
    sauveteurForm: FormGroup;
    submitted = false;

    constructor(private sauvetageListService: SauvetagesListService, public authService: AuthService, private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.sauvetageListService.getAllSauvetages().subscribe(
            data => this.sauveteurList = data,
            error => console.log('oops', error)
        );
        this.sauveteurForm = this.formBuilder.group({
            id: [],
            nom: [''],
            prenom: [''],
            date_de_naissance: [''],
            date_de_mort: [''],
            patronyme: ['', Validators.required],
            fonction: [''],
            decoration: [''],
            divers: ['']
        });
    }

    get f() { return this.sauveteurForm.controls; }

    submit() {
        this.submitted = true;



        // stop here if form is invalid
        if (this.sauveteurForm.invalid) {
            return;
        }

        if (this.sauveteurForm.value.id) {
            this.sauvetageListService.edit(this.sauveteurForm.value).subscribe((data) => console.log(data));
        } else {
            this.sauvetageListService.add(this.sauveteurForm.value).subscribe((data) => console.log(data));
        }



    }

    edit(_data) {
        console.log(_data)
        this.sauveteurForm = this.formBuilder.group({
            id: [_data.ID],
            nom: [_data.NOM],
            prenom: [_data.PRENOM],
            date_de_naissance: [_data.DATE_DE_NAISSANCE],
            date_de_mort: [_data.DATE_DE_MORT],
            patronyme: [_data.PATRONYME, Validators.required],
            fonction: [_data.FONCTION],
            decoration: [_data.DECORATION],
            divers: [_data.DIVERS]
        });
        document.getElementById("addSouveuteurButton").click();
    }

    delete(_body) {
        this.sauvetageListService.delete(_body)
    }


}
