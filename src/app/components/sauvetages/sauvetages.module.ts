
import {NgModule} from '@angular/core';
import {NgxScrollTopModule} from 'ngx-scrolltop';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from '../pages/not-found/not-found.component';
import { SauvetagesListComponent} from './sauvetages-list/sauvetages-list.component';
import {CommonModule} from '@angular/common';
import {SauvetagesListService} from './sauvetages-list/sauvetages-list.service';
import { HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
    {path: '', component: SauvetagesListComponent},
    {path: '**', component: NotFoundComponent} // This line will remain down from the whole pages component list
];

@NgModule({
    declarations: [SauvetagesListComponent],
    imports: [
        CommonModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        NgxScrollTopModule, HttpClientModule
    ],
    providers: [SauvetagesListService]
})
export class SauvetagesModule {
}
